import request from '@/utils/request'

const api_name = '/admin/system/sysRole'


export default {
    /*
     * 获取角色分页列表
     */
    getPageList(page, limit, searchObj) {
      return request({
          url: `${api_name}/${page}/${limit}`,
          method: 'get',
          params: searchObj
      })
    },

    //根据id删除数据    
    removeById(id) {
      return request({
        url: `${api_name}/remove/${id}`,
        method: 'delete'
      })
    },


    //角色添加
    save(role) {
      return request({
        url: `${api_name}/save`,
        method: 'post',
        data: role
      })
    },

    //根据id查询角色
    getById(id) {
      return request({
        url: `${api_name}/get/${id}`,
        method: 'get'
      })
    },
    
    //角色修改
    updateById(role) {
      return request({
        url: `${api_name}/update`,
        method: 'put',
        data: role
      })
    },

    //批量删除
    batchRemove(idList) {
        return request({
          url: `${api_name}/batchRemove`,
          method: `delete`,
          data: idList
        })
    },

    //根据用户id查询角色数据
    getRoles(userId) {
      return request({
        url: `${api_name}/toAssign/${userId}`,
        method: 'get'
      })
    },


    //根据用户id分配角色
    assignRoles(assignRole){
      return request({
        url: `${api_name}/doAssign`,
        method: 'post',
        data: assignRole
      })
    },


}